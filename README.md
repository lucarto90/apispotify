Luego de descargar el proyecto, ejecutar los siguientes comandos en la consola:

1) composer install
2) yarn install
3) yarn encore dev, si lo tiene en produccion agregar yarn encore prod

Finalmente ejecutar lo siguiente en la consola

symfony server:start

Cordialmente, <br>
Luis Torres.